<?php
    get_template_part('includes/header'); 
    bk_main_before();
?>
<!-- 
<div class="container pt-5">
  <div class="row">

    <div class="col">
      <div id="content" role="main">
        <?php // get_template_part('includes/loops/page-content'); ?>
      </div>
    </div>
  </div>
</div>/.container -->


  <section class="container pt-5">
  <div class="row">

  <?php
  $home_loop_args = array(
    'post_type'      => 'normativas_sectorial',
    'posts_per_page' => -1,
    'order' => 'DESC'
  );
  $home_loop = new WP_Query( $home_loop_args );
  ?>
  <?php if ( $home_loop->have_posts() ) :?>
    <?php while ($home_loop->have_posts()) : $home_loop->the_post(); $postid = get_the_ID(); ?>
  <div class="col-sm-4">
      <article class="pb-4">
        <hr>
        <h5 class="bk-pregunta--title">
            <?php the_title()?>
        </h5>
        <hr>
            <p>
            <?php the_content()?>
            </p>
      </article>
  </div>
    <?php endwhile; wp_reset_postdata();?>

  <?php else : get_template_part('./includes/loops/404');
  endif; ?>
  </div>
</section>

<?php 
    bk_main_after();
    get_template_part('includes/footer'); 
?>