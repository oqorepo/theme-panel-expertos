<?php
/**
 * Template Name: Institucion
*/
    get_template_part('includes/header'); 
    bk_main_before();
?>
<div class="container pt-5">
  <div class="row">

    <div class="col">
      <div id="content" role="main">
        <?php get_template_part('includes/loops/page-content'); ?>
      </div>
    </div>
  </div>
</div>
<section class="p-5 container-fluid bk-hide--marco">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p class=""><strong>Normativa Interna Panel de Expertos</strong>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'normativa-interna'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                            $icon_doc = get_sub_field('icono');
                            $file = get_sub_field('archivo');
                        ?>
                        <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                            <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                            <?php
                            echo the_title(); 
                            ?>
                        </a>
                        <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-6">
                <p class=""><strong>Acuerdos</strong>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'acuerdos'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                            $icon_doc = get_sub_field('icono');
                            $file = get_sub_field('archivo');
                        ?>
                        <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                            <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                            <?php
                            echo the_title(); 
                            ?>
                        </a>
                        <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section class="pt-5 pb-5 container-fluid bk-section--institucion" id="organigrama">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h3 class="bk--primary-color">Composición</h3>
                <div class="bk-button--default">
                    <button class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-modern vc_btn3-color-grey" id="bk-hide--composicion">
                    Ver más
                    </button>
                </div>
            </div>
            <div class="col-sm-9">
            <?php if(have_posts()): while(have_posts()): the_post(); ?>
                <!-- <p>El Panel de Expertos estará integrado por siete profesionales, cinco de los cuales deberán ser ingenieros o licenciados en ciencias económicas, nacionales o extranjeros, y dos abogados, de amplia trayectoria profesional o académica y que acrediten, en materias técnicas, económicas o jurídicas del sector energético, dominio y experiencia laboral mínima de tres años</p>
                <p>El Panel de Expertos cuenta con un Secretario Abogado, designado por el Tribunal de Defensa de la Libre Competencia, mediante concurso público de antecedentes, por un período de seis años.</p> -->
                <?php the_field('sobre_el_panel'); ?>
            <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid text-center bk-hide--composicion bk-section--institucion">
    <div class="container  bk-line-image">
    <div class="row">
        <h3 class="pt-4 pb-4 bk-pregunta--title bk--primary-color">Organigrama</h3>
        <ul class="d-md-flex justify-content-around bk--primary-color">
        
        <?php
        $home_loop_args = array(
            'post_type'      => 'integrantes',
            'posts_per_page' => 17,
            'tax_query' => array(
                array(
                    'taxonomy' => 'cargo',
                    'field' => 'slug',
                    'terms' => 'integrante'
                )
            )
        );
        $home_loop = new WP_Query( $home_loop_args );
        ?>
        <?php if ( $home_loop->have_posts() ) :?>
            <?php while ($home_loop->have_posts()) : $home_loop->the_post(); ?>
            
            <li class="m-2 mb-0 flex-fill bd-highlight modalShow">
                <img class="director" src="<?php the_post_thumbnail_url(); ?>" alt="">
                <p><small class="titulo"><?php the_title(); ?></small></p>
                <?php if( get_field('cargo_actual') ): ?>
                <p><small><?php the_field('cargo_actual'); ?></small></p>
                <?php endif; ?>
                <div class="d-none resumen"><?php the_content(); ?></div>
                
            </li>

            <?php endwhile; wp_reset_postdata();?>

        <?php endif; ?>
        
        </ul>
    </div>
    <div class="row">
        <ul class="w-100 d-md-flex justify-content-center bk--primary-color">
            
        <?php $home_loop_args = array(
            'post_type'      => 'integrantes',
            'posts_per_page' => 1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'cargo',
                    'field' => 'slug',
                    'terms' => 'secretaria'
                )
            )
        );
        $home_loop = new WP_Query( $home_loop_args );
        ?>
        <?php if ( $home_loop->have_posts() ) :?>
            <?php while ($home_loop->have_posts()) : $home_loop->the_post(); ?>
            
            <li class="m-2 mb-0 bd-highlight modalShow" style="max-width:150px;">
                <img class="director" src="<?php the_post_thumbnail_url(); ?>" alt="">
                <p><small class="titulo"><?php the_title(); ?></small></p>
                
                <?php if( get_field('cargo_actual') ): ?>
                <p><small><?php the_field('cargo_actual'); ?></small></p>
                <?php endif; ?>

                <div class="d-none resumen"><?php the_content(); ?></div>
            </li>

            <?php endwhile; wp_reset_postdata();?>

        <?php endif; ?>

        </ul >
    </div>
    <div class="row">
         <ul class="w-100 d-md-flex justify-content-center bk--primary-color">
            
        <?php
        $home_loop_args = array(
            'post_type'      => 'integrantes',
            'posts_per_page' => 1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'cargo',
                    'field' => 'slug',
                    'terms' => 'jefe-administrativo'
                )
            )
        );
        $home_loop = new WP_Query( $home_loop_args );
        ?>
        <?php if ( $home_loop->have_posts() ) :?>
            <?php while ($home_loop->have_posts()) : $home_loop->the_post(); ?>
            
            <li class="p-2 m-2 mb-0 bd-highlight">
                <img class="director" src="<?php the_post_thumbnail_url(); ?>" alt="">
                <p><small class="titulo"><?php the_title(); ?></small></p>
                <?php if( get_field('cargo_actual') ): ?>
                <p><small><?php the_field('cargo_actual'); ?></small></p>
                <?php endif; ?>
                <div class="d-none resumen"><?php the_content(); ?></div>
            </li>

            <?php endwhile; wp_reset_postdata();?>

        <?php endif; ?>
            
        </ul >
    </div>
    <div class="row">
        <ul class="w-100 d-flex justify-content-center bk--primary-color">
            
        <?php
        $home_loop_args = array(
            'post_type'      => 'integrantes',
            'posts_per_page' => 3,
            'tax_query' => array(
                array(
                    'taxonomy' => 'cargo',
                    'field' => 'slug',
                    'terms' => 'administrativos'
                )
            )
        );
        $home_loop = new WP_Query( $home_loop_args );
        ?>
        <?php if ( $home_loop->have_posts() ) :?>
            <?php while ($home_loop->have_posts()) : $home_loop->the_post(); ?>
            
            <li class="p-2 m-2 mb-0 bd-highlight">
                <img class="director" src="<?php the_post_thumbnail_url(); ?>" alt="">
                <p><small class="titulo"><?php the_title(); ?></small></p>
                <?php if( get_field('cargo_actual') ): ?>
                <p><small><?php the_field('cargo_actual'); ?></small></p>
                <?php endif; ?>
                <div class="d-none resumen"><?php the_content(); ?></div>
                
            </li>

            <?php endwhile; wp_reset_postdata();?>

        <?php endif; ?>
            
        </ul >
    </div>
  </div>
    <hr>
    <div class="container text-left">
        <h3 class="bk--primary-color pb-3 ">Panelistas anteriores</h3>
        <div class="row">
        <?php the_field('panelistas_anteriores'); ?>
        </div>
    </div>
</section>
<section id="informe" class="p-5 container-fluid bk-hide--informes">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="bk--primary-color pb-3 text-left">Informes de actividades</h3>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'informe-de-actividades'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                            $icon_doc = get_sub_field('icono');
                            $file = get_sub_field('archivo');
                        ?>
                            <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                                <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                                <?php
                                echo the_title(); 
                                ?>
                            </a>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-6">
                <h3 class="bk--primary-color pb-3 text-left">Registro de asistencia</h3>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => 2,
                    'category_name'  => 'registro-de-asistencia'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                                $icon_doc = get_sub_field('icono');
                                $file = get_sub_field('archivo');
                            ?>
                            <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                                <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                                <?php
                                echo the_title(); 
                                ?>
                            </a>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
    bk_main_after();
    get_template_part('includes/footer'); 
?>