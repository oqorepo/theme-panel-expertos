/**!
 * bk JS
 */

(function ($) {

	'use strict';

	$(document).ready(function () {

		/*	------------------------------------------------------------------
			PRELOAD
		------------------------------------------------------------------
		*/
		$(window).on('load', function () {
			$('.cd-loader').fadeOut('slow', function () {
				$(this).remove();
			});
		});

		// Comments

		$('.commentlist li').addClass('card');
		$('.comment-reply-link').addClass('btn btn-secondary');

		// Forms

		$('select, input[type=text], input[type=email], input[type=password], textarea').addClass('form-control');
		$('input[type=submit]').addClass('btn btn-primary');

		// Pagination fix for ellipsis

		$('.pagination .dots').addClass('page-link').parent().addClass('disabled');

		// You can put your own code in here

		var $lateral_menu_trigger = $('#bk-menu-trigger'),
			$content_wrapper = $('.bk-main-content'),
			$navigation = $('.bk-header');

		function menu_action(){
			$lateral_menu_trigger.toggleClass('is-clicked');
			$navigation.toggleClass('lateral-menu-is-open');
			$content_wrapper.toggleClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
				// firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
				$('body').toggleClass('overflow-hidden');
			});
			$('#bk-lateral-nav').toggleClass('lateral-menu-is-open');
	
			//check if transitions are not supported - i.e. in IE9
			if ($('html').hasClass('no-csstransitions')) {
				$('body').toggleClass('overflow-hidden');
			}
		}

		$('#bk-close-menu-trigger').on('click', function (){
			menu_action();
		});

		//open-close lateral menu clicking on the menu icon
		$lateral_menu_trigger.on('click', function (event) {
			event.preventDefault();

			menu_action();
		});

		//close lateral menu clicking outside the menu itself
		$content_wrapper.on('click', function (event) {
			if (!$(event.target).is('#bk-menu-trigger, #bk-menu-trigger span')) {
				$lateral_menu_trigger.removeClass('is-clicked');
				$navigation.removeClass('lateral-menu-is-open');
				$content_wrapper.removeClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function () {
					$('body').removeClass('overflow-hidden');
				});
				$('#bk-lateral-nav').removeClass('lateral-menu-is-open');
				//check if transitions are not supported
				if ($('html').hasClass('no-csstransitions')) {
					$('body').removeClass('overflow-hidden');
				}

			}
		});

		//open (or close) submenu items in the lateral menu. Close all the other open submenu items.
		$('.item-has-children').children('a').on('click', function (event) {
			event.preventDefault();
			$(this).toggleClass('submenu-open').next('.sub-menu').slideToggle(200).end().parent('.item-has-children').siblings('.item-has-children').children('a').removeClass('submenu-open').next('.sub-menu').slideUp(200);
		});

		/*	------------------------------------------------------------------
		Esconder logo
		------------------------------------------------------------------
		*/
		$(window).on('scroll', function () {

			if ($(window).scrollTop() > 300) {
				$('.bk-header').fadeOut();
				$('.bk-navbar--logo').fadeOut();
			} else {
				$('.bk-header').fadeIn();
				$('.bk-navbar--logo').fadeIn();
			}
		});
		/*	------------------------------------------------------------------
		Tramitadas
		------------------------------------------------------------------
		*/
		$('.bk-discre-tra').hide();
		$('.bk-discre-tra').last().show();

		$('.bk-discre-tra').each(function(){
			let y = $(this).find('h3').text();
			$(this).attr('id', 'bk-discre-tra-'+ y +'' );
			let x = $(this).attr('id');
			$('.bk-discre-tra--ul').append('<li class="col-sm-2 bk-discre-tra--li '+ x +'"><a class="bk-discre-tra--btn" id="'+ x +'">'+ y +'</li>');
		});

		$('.bk-discre-tra--btn').last().parent().addClass('active');

		$('.bk-discre-tra--btn').on('click', function () {
			let traName = $(this).attr('id');
			console.log(traName);
			$('.bk-discre-tra').hide();
			$('.bk-discre-tra--li').removeClass('active');
			$(this).parent().addClass('active');
			$('.bk-discre-tra').each(function () {
				$('.' + traName).show();
			});
		});


		// Back to Top - by CodyHouse.co
		var backTop = document.getElementsByClassName('js-cd-top')[0],
			// browser window scroll (in pixels) after which the "back to top" link is shown
			offset = 300,
			//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
			offsetOpacity = 1200,
			scrollDuration = 700,
			scrolling = false;
		if (backTop) {
			//update back to top visibility on scrolling
			window.addEventListener("scroll", function (event) {
				if (!scrolling) {
					scrolling = true;
					(!window.requestAnimationFrame) ? setTimeout(checkBackToTop, 250): window.requestAnimationFrame(checkBackToTop);
				}
			});
			//smooth scroll to top
			backTop.addEventListener('click', function (event) {
				event.preventDefault();
				(!window.requestAnimationFrame) ? window.scrollTo(0, 0): scrollTop(scrollDuration);
			});
		}

		function checkBackToTop() {
			var windowTop = window.scrollY || document.documentElement.scrollTop;
			(windowTop > offset) ? addClass(backTop, 'cd-top--show'): removeClass(backTop, 'cd-top--show', 'cd-top--fade-out');
			(windowTop > offsetOpacity) && addClass(backTop, 'cd-top--fade-out');
			scrolling = false;
		}

		function scrollTop(duration) {
			var start = window.scrollY || document.documentElement.scrollTop,
				currentTime = null;

			var animateScroll = function (timestamp) {
				if (!currentTime) currentTime = timestamp;
				var progress = timestamp - currentTime;
				var val = Math.max(Math.easeInOutQuad(progress, start, -start, duration), 0);
				window.scrollTo(0, val);
				if (progress < duration) {
					window.requestAnimationFrame(animateScroll);
				}
			};

			window.requestAnimationFrame(animateScroll);
		}

		Math.easeInOutQuad = function (t, b, c, d) {
			t /= d / 2;
			if (t < 1) return c / 2 * t * t + b;
			t--;
			return -c / 2 * (t * (t - 2) - 1) + b;
		};

		//class manipulations - needed if classList is not supported
		function hasClass(el, className) {
			if (el.classList) return el.classList.contains(className);
			else return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
		}

		function addClass(el, className) {
			var classList = className.split(' ');
			if (el.classList) el.classList.add(classList[0]);
			else if (!hasClass(el, classList[0])) el.className += " " + classList[0];
			if (classList.length > 1) addClass(el, classList.slice(1).join(' '));
		}

		function removeClass(el, className) {
			var classList = className.split(' ');
			if (el.classList) el.classList.remove(classList[0]);
			else if (hasClass(el, classList[0])) {
				var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
				el.className = el.className.replace(reg, ' ');
			}
			if (classList.length > 1) removeClass(el, classList.slice(1).join(' '));
		}

		let thisID = $('.collapse-btn').first().attr('href');
		$(thisID).collapse({
			toggle: true
		});
		
		$('.collapse-btn').each( function() {
			let ariaAttr = $(this).first().attr('aria-expanded');
			console.log(ariaAttr);
			console.log(typeof(ariaAttr));
			if ( ariaAttr === 'false' ){
				$('.collapse-icon').addClass('fa-minus-circle');
			}else{
				$('.collapse-icon').addClass('fa-plus-circle');
			}
		});
		//$('.collapse-btn .collapse-icon').first().addClass('fa-plus-circle');

		$(".tribe-events-page-title").text(function () {
			return $(this).text().replace("Eventos en", "Calendario de Sesiones"); 
		});
		$('#tribe-events-content-wrapper').append('<div class="container"><div class="row"><div class="col"><p class="text-center">La planificación futura de las Sesiones está sujeta a modificaciones</p></div></div></div>')

		$('input[name="contacto-telefono"]').addClass('form-control');
		$('input[name="contacto-rut"]').addClass('form-control');

		$('.modalShow').click(function(event){
			event.preventDefault();
			var e = $(this),
			image = e.find('.director').attr('src'),
			title = e.find('.titulo').text(),
			body = e.find('.resumen').text();
			$('#modal-img').attr('src', image);
			$('#modal-title').text(title);
			$('#modal-body').text(body);
			$("#myModal").modal("show");
		});

		$('.bk-hide--competencia').hide();
		$('.bk-hide--marco').hide();
		$('.bk-hide--composicion').hide();
		
		$('#bk-hide--competencia').click(function (){
			var link = $('#bk-hide--competencia button');
			$('.bk-hide--competencia').slideToggle(function(){
				if ($(this).is(':visible')){
					link.text('Contraer');
				}else{
					link.text('Ver más');
				}
			});
			
		});
		$('#bk-hide--marco').click(function(){
			var link = $('#bk-hide--marco button');
			$('.bk-hide--marco').slideToggle(function(){
				if ($(this).is(':visible')){
					link.text('Contraer');
				}else{
					link.text('Ver más');
				}
			});
			
		});
		$('#bk-hide--composicion').click(function(){
			var link = $(this);
			$('.bk-hide--composicion').slideToggle(function(){
				if ($(this).is(':visible')){
					link.text('Contraer');
				}else{
					link.text('Ver más');
				}
			});
		});
		$('#bk-hide--informes').click(function(){
			 var link = $('#bk-hide--informes button');
			$('.bk-hide--informes').slideToggle(function(){
				if ($(this).is(':visible')){
					link.text('Contraer');
				}else{
					link.text('Ver más');
				}
			});
		});

		//$('.bk-d-parent a').attr('data-target', '#');
		function getParameterByName(name, url) {
			if (!url) url = window.location.href;
			name = name.replace(/[\[\]]/g, "\\$&");
			var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
			if (!results) return null;
			if (!results[2]) return '';
			return decodeURIComponent(results[2].replace(/\+/g, " "));
		}
		var avProjectName = getParameterByName('c');
		if (avProjectName == 'y'){
			$(window).on('load',function(){
				console.log('Funca!');
				$('#contactoModal').modal('show');
			});
		}

		$('.search-div input.search-input').attr('placeholder', 'Buscar dentro de los contenidos');
		
		$("#wpcf7-f5-o1").on('wpcf7submit', function (event) {
			console.log("OK send submit");
			$(this).fadeOut();
			$("#contactoModal .modal-body.container .thanks").fadeIn();
		});

		$("#wpcf7-f5-o1").on('wpcf7mailfailed ', function (event) {
			console.log("NO send mailfailed");
			$("#contactoModal .modal-body.container").append('<p>Ocurrió un error, intente enviar el formulario de nuevo.</p>')
		});
	
	});

}(jQuery));