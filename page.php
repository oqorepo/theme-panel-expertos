<?php
    get_template_part('includes/header'); 
    bk_main_before();
?>

  <?php if (is_page('tramitadas')): ?>
<div class="container pt-5">
  <h2 class="bk--primary-color pt-5">Seleccionar el año a consultar</h2>
  <hr class="pb-4">
  <ul class="row bk-discre-tra--ul">
  </ul>
</div><!-- /.container -->
  <?php endif; ?>



<?php if (is_page('home')): ?>
  <section class="container pt-5">

  <div class="row">

  <div class="col-sm-6">
    <article>
    <h2>La Institución</h2>
    <hr class="pb-4">
    <p>El Panel de Expertos de la Ley General de Servicios Eléctricos es un órgano colegiado autónomo creado en el año 2004 por la Ley Nº 19.940, de competencia estricta y reglada. Su función es pronunciarse, mediante dictámenes de efecto vinculante, sobre aquellas discrepancias y conflictos que, conforme a la ley, se susciten con motivo de la aplicación de la legislación eléctrica y de servicios de gas que las empresas eléctricas, de servicios de gas y otras entidades habilitadas sometan a su conocimiento.</p>
    </article>
  </div>
  <?php
  $home_loop_args = array(
    'post_type'      => 'post',
    'posts_per_page' => 2
  );
  $home_loop = new WP_Query( $home_loop_args );
  ?>
  <div class="col-sm-6">
  <?php if ( $home_loop->have_posts() ) :?>
    <h2>Últimos Comunicados</h2>
    <hr class="pb-4">
    <?php while ($home_loop->have_posts()) : $home_loop->the_post(); ?>
      <article class="">
        <p class=""><i class="far fa-calendar-alt"></i>&nbsp;<?php echo get_the_date('d M, Y'); ?></p>
        <a href="<?php the_permalink();?>">
        <h5>
          <?php the_title()?>
        </h5>
        </a>
        <?php the_excerpt()?>
<!--         <a href="<?php // the_permalink();?>">
          Ver más
        </a> -->
      </article>
      <hr>
    <?php endwhile; wp_reset_postdata();?>

  <?php endif; ?>
  </div>
  </div>
</section>

<?php endif; ?>
<div class="container">
  <div class="row">
      <div class="col">
          <?php get_template_part('includes/loops/page-content'); ?>
      </div>
    </div>
</div>
<?php 
    bk_main_after();
    get_template_part('includes/footer'); 
?>