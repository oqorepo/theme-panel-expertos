<?php
    get_template_part('includes/header'); 
    bk_main_before();
?>
  <section class="container pt-5">
  <div class="row">
    <div class="col">
    <?php if(have_posts()) : while(have_posts()) : 
      the_post(); 

      the_content(); 

    endwhile; 
    wp_reset_postdata();
    endif; 
    ?>
    </div>
  </div>
  <div class="row">

  <?php
  $home_loop_args = array(
    'post_type'      => 'documentos',
    'posts_per_page' => -1,
    'category_name'  => 'biblioteca'
  );
  $home_loop = new WP_Query( $home_loop_args );
  ?>
  <?php if ( $home_loop->have_posts() ) :?>
    <?php while ($home_loop->have_posts()) : $home_loop->the_post(); $postid = get_the_ID(); ?>
  <div class="col-sm-4">
      <?php if( have_rows('documentos') ):
          while( have_rows('documentos') ): the_row();
          $icon_doc = get_sub_field('icono');
          $file = get_sub_field('archivo');
      ?>
          <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
              <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
              <?php
              echo the_title(); 
              ?>
          </a>
          <?php endwhile; ?>
      <?php endif; ?>
  </div>
    <?php endwhile; wp_reset_postdata();?>

  <?php else : get_template_part('./includes/loops/404');
  endif; ?>
  </div>
</section>

<?php 
    bk_main_after();
    get_template_part('includes/footer'); 
?>