<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div class="cd-loader">
    <small>...</small>
    <div class="cd-loader__grid">
      <div class="cd-loader__item"></div>
    </div>
  </div>

  <?php bk_navbar_before();
  if (is_front_page()):?>
  <a class="bk-navbar--logo" href="<?php echo esc_url( home_url('/') ); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.png" alt="" class="bk-navbar--logo__img"></a>
<?php endif;?>
  <header class="bk-header">
    <a id="bk-menu-trigger" class="" href="#"><span class="bk-menu-text">Menú</span><span class="bk-menu-icon"></span></a>
  </header>
  <nav id="bk-lateral-nav" class="navbar navbar-expand-md bk-navbar">
    <a id="bk-close-menu-trigger" class="d-block d-md-none" href="#"><span class="bk-menu-text">X cerrar</span><span class="bk-menu-icon"></span></a>
    <!-- <nav id="bk-lateral-nav" class="navbar navbar-expand-md navbar-light bg-light"> -->

    <?php //bk_navbar_brand();?>

    <?php
        wp_nav_menu( array(
          'theme_location'  => 'navbar',
          'container'       => false,
          'menu_class'      => '',
          'fallback_cb'     => '__return_false',
          'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          'depth'           => 2,
          'walker'          => new bk_walker_nav_menu()
        ) );
      ?>

  </nav>
  
  <?php bk_navbar_after();?>
  
  <main id="main" class="bk-main-content">
    
    <?php if (is_page('home')):?>
    <div class="">
      <?php echo do_shortcode('[rev_slider alias="thirty-eighth"]'); ?>
    </div>

        <div class="container">
      <nav class="bk-second-nav">
          <?php //bk_navbar_search();?>
        <ul class="d-sm-flex justify-content-end bk-second-nav--ul">
          <li class="align-self-center m-2 p-4 bk-second-nav--li">
            <a href="<?php echo esc_url( home_url( 'discrepancias/en-curso/' ) ); ?>" class="bk-second-nav--link">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-en-curso.png" alt="">
              <small class="d-block pt-2">Discrepancias <br> en Curso</small>
            </a>
          </li>
          <li class="align-self-center m-2 p-4 bk-second-nav--li">
            <a href="<?php echo esc_url( home_url( 'discrepancias/tramitadas/' ) ); ?>" class="bk-second-nav--link">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-tramitadas.png" alt="">
              <small class="d-block pt-2">Discrepancias <br> Tramitadas</small>
            </a>
          </li>
          <li class="align-self-center m-2 p-4 bk-second-nav--li">
            <a href="<?php echo esc_url( home_url( 'preguntas-frecuentes/' ) ); ?>" class="bk-second-nav--link">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/bk-preguntas.png" alt="">
              <small class="d-block pt-2">Preguntas <br> Frecuentes</small>
            </a>
          </li>
        </ul>
      </nav>
    </div>

    <?php elseif (is_page()):
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
    ?>
    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
    <?php if(is_page(array('en-curso','tramitadas'))){
    $parent_post_id = 19;
    $parent_post = get_post($parent_post_id);
    $parent_post_title = $parent_post->post_title;}
    
    //the_post_thumbnail('full', array('class'=> 'position-relative')); ?>
    <div class="container-fluid" id="post" style="background: #343a40 url('<?php echo $thumb['0'];?>') bottom center;background-size:cover; min-height:550px;">
      <div class="container">
        <div class="row">
          <div class="col-sm">
            <div class="d-flex align-items-center" style="min-height:550px;">
              <h3 class="text-white bk-page--title bk-title--shadow mt-4">
                <?php 
                if(is_page(array('en-curso','tramitadas'))){
                  echo $parent_post_title." ";
                }
                the_title();
                ?>
              </h3>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endwhile; endif; ?>
    
    <?php 
    elseif (is_singular('tribe_events')):
      echo "Funca";
    
    else: 
      echo do_shortcode('[rev_slider alias="thirty-eighth"]'); ?>

    <?php endif; ?>