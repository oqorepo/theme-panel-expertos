<?php bk_footer_before();?>
<footer id="footer" class="bk-footer bg-dark">

  <div class="container pt-5 pb-5 mt-5">

    <?php if(is_active_sidebar('footer-widget-area')): ?>
    <div class="row pt-5 pb-4" id="footer" role="navigation">
      <?php dynamic_sidebar('footer-widget-area'); ?>
    </div>
    <?php endif; ?>

    <div class="row text-white">
      <div class="col-sm-4">
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.png" alt="Panel expertos">
      </div>
      <div class="col-sm-4">
        <h6><i class="fas fa-map-marker-alt"></i> Dirección</h6>
        <hr>
        <p>Carmencita N°25, Oficina 101, Las Condes.</p>
      </div>
      <div class="col-sm-4">
        <h6><i class="far fa-envelope"></i> Contacto</h6>
        <hr>
        <p><a href="mailto:contacto@panelexpertos.cl" class="text-white">contacto@panelexpertos.cl</a></p>
      </div>
    </div>

  </div>
  
</footer>
<?php bk_bottomline();?>
<a href="#0" class="cd-top js-cd-top">Top</a>
</main>
<?php bk_footer_after();?>
<div class="modal fade" id="contactoModal" tabindex="-1" role="dialog" aria-labelledby="contactoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h5 class="modal-title" id="contactoModalLabel">¿QUIERES SABER ALGO MÁS?</h5> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body container">
                <!-- <p>¡CONTÁCTANOS! Si tienes dudas o consultas, llena nuestro formulario y te responderemos a la brevedad.</p> -->
                <?php echo do_shortcode( '[contact-form-7 id="5" title="Formulario de contacto 1"]' ); ?>
                <div class="thanks text-center" style="display: none;">
                <h2><i class="fas fa-envelope-open-text" style></i></h2>
                  <h3>Gracias <span class="nombre-persona"></span></h3>
                  <p><strong>Tu solicitud ha sido enviada con éxito.</strong></p>
                  <p>Pronto te contactaremos.</p>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="contactoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body container">
              <div class="row">
                <div class="col-sm-4">
                  <img src="" alt="" id="modal-img">
                </div>
                <div class="col-sm-8">
                  <h4 class="modal-title" id="modal-title"></h4>
                  <hr>
                  <p id="modal-body"></p>
                </div>
              </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

<?php wp_footer(); ?>
</body>
</html>
