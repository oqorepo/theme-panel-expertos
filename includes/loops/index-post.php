<?php
/*
The Index Post (or excerpt)
===========================
Used by index.php, category.php and author.php
*/
?>

<article role="article" id="post_<?php the_ID()?>" <?php post_class("mt-5 mb-5"); ?> >
  <header>
    <h4>
      <a href="<?php the_permalink(); ?>">
        <?php the_title()?>
      </a>
    </h4>
    <p class="text-muted">
      <i class="far fa-calendar-alt"></i>&nbsp;<?php bk_post_date(); ?>&nbsp;
    </p>
  </header>
  <div>
    <?php the_post_thumbnail(); ?>

    <?php if ( has_excerpt( $post->ID ) ) {
    the_excerpt();
    ?><a href="<?php the_permalink(); ?>">
    	<?php _e( 'Continue reading...', 'bk' ) ?>
      </a>
  	<?php } else {
  	  the_content( __('Continue reading...', 'bk' ) );
	  } ?>
  </div>
</article>
