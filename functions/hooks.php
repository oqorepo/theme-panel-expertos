<?php
/*
bk Hooks
==========
Designed to be used by a child theme.
*/

// Navbar (in `header.php`)

function bk_navbar_before() {
  do_action('navbar_before');
}

function bk_navbar_after() {
  do_action('navbar_after');
}
function bk_navbar_brand() {
  if ( ! has_action('navbar_brand') ) {
    ?>
<a class="navbar-brand" href="<?php echo esc_url( home_url('/') ); ?>">
  <?php bloginfo('name'); ?></a>
<?php
  } else {
		do_action('navbar_brand');
	}
}
function bk_navbar_search() {
  if ( ! has_action('navbar_search') ) {
    ?>
<form class="form-inline ml-auto pt-2 pt-md-0" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
  <input class="form-control mr-sm-1" type="text" value="<?php echo get_search_query(); ?>" placeholder="Buscar..."
    name="s" id="s">
  <button type="submit" id="searchsubmit" value="<?php esc_attr_x('Search', 'bk') ?>" class="btn btn-outline-secondary my-2 my-sm-0">
    <i class="fas fa-search"></i>
  </button>
</form>
<?php
  } else {
		do_action('navbar_search');
	}
}


// Main

function bk_main_before() {
  do_action('main_before');
}
function bk_main_after() {
  do_action('main_after');
}

// Sidebar (in `sidebar.php` -- only displayed when sidebar has 1 widget or more)

function bk_sidebar_before() {
  do_action('sidebar_before');
}
function bk_sidebar_after() {
  do_action('sidebar_after');
}

// Footer (in `footer.php`)

function bk_footer_before() {
  do_action('footer_before');
}
function bk_footer_after() {
  do_action('footer_after');
}
function bk_bottomline() {
	if ( ! has_action('bottomline') ) {
		?>
  <div class="container-fluid bk-footer--after">
    <div class="container pt-3 pb-3">
      <div class="row">
        <div class="col-sm text-center">
          <small class="text-sm-left text-white">Esta página Web cumple con lo establecido en la Ley Nº 20.285 y en la Ley N° 20.730. &copy;
            <?php echo date('Y'); ?> <a href="<?php echo home_url('/'); ?>" class="text-white">
              <?php bloginfo('name'); ?></a></small>
        </div>
      </div>
    </div>
  </div>
<?php		
	} else {
		do_action('bottomline');
	}
}