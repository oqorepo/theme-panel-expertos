<?php

function preguntas_frecuentes_cpt() {

	$labels = array(
		'name'                  => _x( 'Preguntas', 'General Name', 'text_domain' ),
		'singular_name'         => _x( 'Pregunta', 'Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Preguntas Frecuentes', 'text_domain' ),
		'name_admin_bar'        => __( 'Preguntas Frecuentes', 'text_domain' ),
		'archives'              => __( 'Archivos', 'text_domain' ),
		'attributes'            => __( 'Atributos', 'text_domain' ),
		'parent_item_colon'     => __( 'Item padre:', 'text_domain' ),
		'all_items'             => __( 'Todas las preguntas frecuentes', 'text_domain' ),
		'add_new_item'          => __( 'Agregar nueva pregunta', 'text_domain' ),
		'add_new'               => __( 'Agregar nueva', 'text_domain' ),
		'new_item'              => __( 'Nuevo Item', 'text_domain' ),
		'edit_item'             => __( 'Editar Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'Ver Item', 'text_domain' ),
		'view_items'            => __( 'Ver Items', 'text_domain' ),
		'search_items'          => __( 'Buscar Item', 'text_domain' ),
		'not_found'             => __( 'No encontrado', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
		'set_featured_image'    => __( 'Establecer imagen destacada', 'text_domain' ),
		'remove_featured_image' => __( 'Remover imagen destacada', 'text_domain' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'text_domain' ),
		'insert_into_item'      => __( 'Insertar dentro de item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Cargados en este item', 'text_domain' ),
		'items_list'            => __( 'lista de Items', 'text_domain' ),
		'items_list_navigation' => __( 'lista de Items de navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filtrar lista de items', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Pregunta', 'text_domain' ),
		'description'           => __( 'Seccion de preguntas frecuentes', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-editor-help',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'preguntas_frecuentes', $args );

}
add_action( 'init', 'preguntas_frecuentes_cpt', 0 );


function normativa_cpt() {

	$labels = array(
		'name'                  => _x( 'Normativas Sectoriales', 'General Name', 'text_domain' ),
		'singular_name'         => _x( 'Normativa Sectorial', 'Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Normativas Sectoriales', 'text_domain' ),
		'name_admin_bar'        => __( 'Normativa Sectorial', 'text_domain' ),
		'archives'              => __( 'Archivos', 'text_domain' ),
		'attributes'            => __( 'Atributos', 'text_domain' ),
		'parent_item_colon'     => __( 'Item padre:', 'text_domain' ),
		'all_items'             => __( 'Todas las normativas sectoriales', 'text_domain' ),
		'add_new_item'          => __( 'Agregar nueva normativa', 'text_domain' ),
		'add_new'               => __( 'Agregar nueva', 'text_domain' ),
		'new_item'              => __( 'Nuevo Item', 'text_domain' ),
		'edit_item'             => __( 'Editar Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'Ver Item', 'text_domain' ),
		'view_items'            => __( 'Ver Items', 'text_domain' ),
		'search_items'          => __( 'Buscar Item', 'text_domain' ),
		'not_found'             => __( 'No encontrado', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
		'set_featured_image'    => __( 'Establecer imagen destacada', 'text_domain' ),
		'remove_featured_image' => __( 'Remover imagen destacada', 'text_domain' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'text_domain' ),
		'insert_into_item'      => __( 'Insertar dentro de item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Cargados en este item', 'text_domain' ),
		'items_list'            => __( 'lista de Items', 'text_domain' ),
		'items_list_navigation' => __( 'lista de Items de navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filtrar lista de items', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Normativa Sectorial', 'text_domain' ),
		'description'           => __( 'Normativa Sectorial Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-list-view',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'normativas_sectorial', $args );

}
add_action( 'init', 'normativa_cpt', 0 );

function documento_cpt() {

	$labels = array(
		'name'                  => _x( 'Documentos', 'General Name', 'text_domain' ),
		'singular_name'         => _x( 'Documento', 'Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Documentos', 'text_domain' ),
		'name_admin_bar'        => __( 'Documentos', 'text_domain' ),
		'archives'              => __( 'Archivos', 'text_domain' ),
		'attributes'            => __( 'Atributos', 'text_domain' ),
		'parent_item_colon'     => __( 'Item padre:', 'text_domain' ),
		'all_items'             => __( 'Todos los documentos', 'text_domain' ),
		'add_new_item'          => __( 'Agregar nuevo documento', 'text_domain' ),
		'add_new'               => __( 'Agregar nuevo', 'text_domain' ),
		'new_item'              => __( 'Nuevo Item', 'text_domain' ),
		'edit_item'             => __( 'Editar Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'Ver Item', 'text_domain' ),
		'view_items'            => __( 'Ver Items', 'text_domain' ),
		'search_items'          => __( 'Buscar Item', 'text_domain' ),
		'not_found'             => __( 'No encontrado', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
		'set_featured_image'    => __( 'Establecer imagen destacada', 'text_domain' ),
		'remove_featured_image' => __( 'Remover imagen destacada', 'text_domain' ),
		'use_featured_image'    => __( 'Usar como imagen destacada', 'text_domain' ),
		'insert_into_item'      => __( 'Insertar dentro de item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Cargados en este item', 'text_domain' ),
		'items_list'            => __( 'lista de Items', 'text_domain' ),
		'items_list_navigation' => __( 'lista de Items de navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filtrar lista de items', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Documentos', 'text_domain' ),
		'description'           => __( 'Documentos Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail'  ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-welcome-add-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'documentos', $args );

}
add_action( 'init', 'documento_cpt', 0 );

// Integrantes del paneñ
add_action( 'init', 'register_cpt_integrantes' );

function register_cpt_integrantes() {

	$labels = array(
		'name' => __( 'Integrantes', 'Integrantes' ),
		'singular_name' => __( 'Integrante', 'Integrantes' ),
		'add_new' => __( 'Agregar', 'Integrantes' ),
		'add_new_item' => __( 'Agregar', 'Integrantes' ),
		'edit_item' => __( 'Editar', 'Integrantes' ),
		'new_item' => __( 'Nuevo', 'Integrantes' ),
		'view_item' => __( 'Ver', 'Integrantes' ),
		'search_items' => __( 'Buscar', 'Integrantes' ),
		'not_found' => __( 'Sin Resultados', 'Integrantes' ),
		'not_found_in_trash' => __( 'Sin Resultados', 'Integrantes' ),
		'parent_item_colon' => __( 'Proyectos', 'Integrantes' ),
		'menu_name' => __( 'Integrantes', 'Integrantes' ),
	
	);

	$args = array(
		'labels'                => $labels,
		'supports'              => array( 'title', 'revisions', 'editor', 'revisions', 'thumbnail'  ),
		'taxonomies'            => array( 'cargo' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-users',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'query_var' => true,
		'can_export' => true,
	);

	register_post_type( 'integrantes', $args );
}

// Register Custom Taxonomy
function cargo_tx() {

	$labels = array(
		'name'                       => _x( 'Cargos', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Cargo', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Cargo', 'text_domain' ),
		'all_items'                  => __( 'Todos los cargos', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Agregar nuevo cargo', 'text_domain' ),
		'edit_item'                  => __( 'Editar cargo', 'text_domain' ),
		'update_item'                => __( 'Update cargo', 'text_domain' ),
		'view_item'                  => __( 'View cargo', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
		'no_terms'                   => __( 'No items', 'text_domain' ),
		'items_list'                 => __( 'Items list', 'text_domain' ),
		'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'cargo', array( 'post' ), $args );

}
add_action( 'init', 'cargo_tx', 0 );