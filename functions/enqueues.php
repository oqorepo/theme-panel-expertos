<?php
/**!
 * Enqueues
 */
$url = 'https://code.jquery.com/jquery-latest.min.js';
$test_url = @fopen($url,'r');
if($test_url !== false) {
	function load_external_jQuery() {
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://code.jquery.com/jquery-latest.min.js');
		wp_enqueue_script('jquery');
	}
	add_action('wp_enqueue_scripts', 'load_external_jQuery');
} else {
	function load_local_jQuery() {
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_bloginfo('template_url').'./assets/js/jquery.min.js', __FILE__, false, '1.11.3', true);
		wp_enqueue_script('jquery');
	}
	add_action('wp_enqueue_scripts', 'load_local_jQuery');
}

if ( ! function_exists('bk_enqueues') ) {
	function bk_enqueues() {

		// Styles

		wp_register_style('bootstrap-css', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css', false, '4.1.3', null);
		wp_enqueue_style('bootstrap-css');

		wp_register_style('fontawesome5-css', 'https://use.fontawesome.com/releases/v5.4.1/css/all.css', false, '5.4.1', null);
		wp_enqueue_style('fontawesome5-css');

		wp_register_style('bk-css', get_template_directory_uri() . '/assets/css/bk.css', false, null);
		wp_enqueue_style('bk-css');

		wp_register_style('bk-main-css', get_template_directory_uri() . '/assets/css/main.css', false, null);
		wp_enqueue_style('bk-main-css');

		// Scripts

		wp_register_script('modernizr',  'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', false, '2.8.3', true);
		wp_enqueue_script('modernizr');

		wp_register_script('popper',  'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', false, '1.14.3', true);
		wp_enqueue_script('popper');

		wp_register_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js', false, '4.1.3', true);
		wp_enqueue_script('bootstrap-js');

		wp_register_script('bk-js', get_template_directory_uri() . '/assets/js/bk.js', false, null, true);
		wp_enqueue_script('bk-js');

		if (is_singular() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}
	}
}
add_action('wp_enqueue_scripts', 'bk_enqueues', 100);
