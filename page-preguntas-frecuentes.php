<?php
    get_template_part('includes/header'); 
    bk_main_before();
?>
<!-- 
<div class="container pt-5">
  <div class="row">

    <div class="col">
      <div id="content" role="main">
        <?php // get_template_part('includes/loops/page-content'); ?>
      </div>
    </div>
  </div>
</div>/.container -->


  <section class="container pt-5">
  <div class="row">

  <?php
  $home_loop_args = array(
    'post_type'      => 'preguntas_frecuentes',
    'posts_per_page' => -1
  );
  $home_loop = new WP_Query( $home_loop_args );
  ?>
  <div class="col">
  <?php if ( $home_loop->have_posts() ) :?>
    <?php while ($home_loop->have_posts()) : $home_loop->the_post(); $postid = get_the_ID(); ?>
      <article class="pb-5">
        <a href="<?php echo '#collapse_'.$postid ?>" class="collapse-btn collapsed" data-toggle="collapse" role="button" aria-expanded="false">
        <h4 class="bk-pregunta--title">
            <i class="fas collapse-icon"></i>
            <?php the_title()?>
        </h4>
        </a>
        <div class="collapse" id="<?php echo 'collapse_'.$postid ?>">
            <p>
            <?php the_content()?>
            </p>
        </div>
      </article>
    <?php endwhile; wp_reset_postdata();?>

  <?php else : get_template_part('./includes/loops/404');
  endif; ?>
  </div>
  </div>
</section>

<?php 
    bk_main_after();
    get_template_part('includes/footer'); 
?>