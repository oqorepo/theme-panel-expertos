<?php
/**
 * Template Name: Transparencia
*/
    get_template_part('includes/header'); 
    bk_main_before();
?>
<div class="container pt-5">
  <div class="row">

    <div class="col">
      <div id="content" role="main">
        <?php get_template_part('includes/loops/page-content'); ?>
      </div>
    </div>
  </div>
</div>

<section class="p-5 container-fluid bk-hide--marco">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p class=""><strong>Normativa</strong></p>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'normativa-interna'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                            $icon_doc = get_sub_field('icono');
                            $file = get_sub_field('archivo');
                        ?>
                            <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                                <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                                <?php
                                echo the_title(); 
                                ?>
                            </a>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-6">
                <p class=""><strong>Acuerdos</strong></p>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'acuerdos'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                            $icon_doc = get_sub_field('icono');
                            $file = get_sub_field('archivo');
                        ?>
                            <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                                <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                                <?php
                                echo the_title(); 
                                ?>
                            </a>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="pt-5 pb-5 container-fluid bk-section--institucion">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h3 class="bk--primary-color">Dotación de personal</h3>
            </div>
            <div class="col-sm-8">
            <?php
        $home_loop_args = array(
            'post_type'      => 'integrantes',
            'posts_per_page' => 4,
            'tax_query' => array(
                array(
                    'taxonomy' => 'cargo',
                    'field' => 'slug',
                    'terms' => array('administrativos', 'jefe-administrativo')
                )
            )
        );
        $home_loop = new WP_Query( $home_loop_args );
        ?>
        <?php if ( $home_loop->have_posts() ) :?>
            <?php while ($home_loop->have_posts()) : $home_loop->the_post(); ?>
            
            
                <p class="cat-cargo"><?php the_title(); ?>,
                <?php if( get_field('cargo_actual') ): ?>
                <small> <u><?php the_field('cargo_actual'); ?></u> </small></p>
                <?php endif; ?>

            <?php endwhile; wp_reset_postdata();?>

        <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<section class="p-5 container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <p class="bk--primary-color pb-3 text-left"><strong>Contratos vigentes</strong></p>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'contratos vigentes'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                            $icon_doc = get_sub_field('icono');
                            $file = get_sub_field('archivo');
                        ?>
                            <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                                <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                                <?php
                                echo the_title(); 
                                ?>
                            </a>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-4">
                <p class="bk--primary-color pb-3 text-left"><strong>Información presupuestaria</strong></p>
                <p>a) Presupuesto</p>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'presupuesto'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                            $icon_doc = get_sub_field('icono');
                            $file = get_sub_field('archivo');
                        ?>
                            <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                                <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                                <?php
                                echo the_title(); 
                                ?>
                            </a>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
                <p>b) Ejecución presupuestaria</p>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'ejecucion-presupuestaria'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                            $icon_doc = get_sub_field('icono');
                            $file = get_sub_field('archivo');
                        ?>
                            <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                                <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                                <?php
                                echo the_title(); 
                                ?>
                            </a>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
                <p>c) Auditoría</p>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'auditoria'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php if( have_rows('documentos') ):
                            while( have_rows('documentos') ): the_row();
                            $icon_doc = get_sub_field('icono');
                            $file = get_sub_field('archivo');
                        ?>
                            <a class="bk-resource-doc" href="<?php echo $file['url']; ?>" target="_blank">
                                <img src="<?php bloginfo('template_directory');?>/assets/img/<?php echo $icon_doc;?>.png" alt="<?php the_title();?>" width="24px" height="24px">
                                <?php
                                echo the_title(); 
                                ?>
                            </a>
                            <?php endwhile; ?>
                        <?php endif; ?>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-4">
                <p class="bk--primary-color pb-3 text-left"><strong>Vínculos a la Ley Lobby y Probidad</strong></p>
                <?php
                $asistencia_args = array(
                    'post_type'      => 'documentos',
                    'posts_per_page' => -1,
                    'category_name'  => 'vinculos-externos'
                );
                $asistencia = new WP_Query( $asistencia_args );
                ?>
                <?php if ( $asistencia->have_posts() ) :?>
                    <?php while ($asistencia->have_posts()) : $asistencia->the_post(); ?>
                        <?php
                        $file = get_field('vinculo_externo');
                        if( $file ):
                        ?>
                        
                        <a class="d-block" href="<?php echo $file; ?>"><?php the_title(); ?></a>
                        <?php endif; ?>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php
    bk_main_after();
    get_template_part('includes/footer'); 
?>